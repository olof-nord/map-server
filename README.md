# map-server

Generate tiles and serve map data for consumption with a web frontend.

## Convert

The original GeoTIFF file needs to be split up into tiles, one possible format being the MBTiles format. 

MBTiles files are essentially SQLite databases containing tiles and metadata tables for storing tilesets.

### MapTiler Desktop

This software provides a GUI, allowing for conversion of GeoTIFF data to be exported to mbtiles format.

The free version has a file size limit for the data to convert, as well as adding a watermark to the converted data. 

https://www.maptiler.com/desktop/

### gdal2mbtiles

This is a python library to convert GDAL readable datasets into MBTiles.

It does not work with my local GeoTIFF data, and errors out with `RuntimeError: OGR Error: Unsupported SRS`.

https://github.com/ecometrica/gdal2mbtiles

```shell
docker-compose up --build gdal2mbtiles
```

### rio-mbtiles

This is a plugin for the Rasterio CLI that exports a raster dataset into MBTiles.

It does also not work with my local GeoTIFF data, and errors out with `rasterio.errors.CRSError: CRS is invalid: None`

https://github.com/mapbox/rio-mbtiles

```shell
docker-compose up --build rio-mbtiles
```

## gdal_translate

The open source Geospatial Data Abstraction Library (GDAL) can transform raster images to many formats.

This seems to be a dependency of many CLI tools. It does successfully create an output without errors with my local
GeoTIFF data.

https://gdal.org/programs/gdal_translate.html

```shell
docker-compose up gdal-translate
```

## Serving the data

### TileServer GL

Simply providing `.mbtiles` files during startup results in a Web UI and a backend which serves the data.

https://github.com/maptiler/tileserver-gl

The served data can be shown using a Leaflet.js `TileLayer`. For react-leaflet, `<TileLayer>` 

### QGIS Server

QGIS Server is an open source WMS, WFS, OGC API for Features 1.0 (WFS3) and WCS implementation that, in addition,
implements advanced cartographic features for thematic mapping.

This server works with `.qgs` project files made by the QGIS Desktop client.

To access the backend, use http://localhost:8380/?SERVICE=WMS&REQUEST=GetCapabilities

The served data can be shown using a Leaflet.js `TileLayer.WMS`. For react-leaflet, `<WMSTileLayer>`.

This approach runs slower than TileServer GL.

## References

* [GitHub: Creating Raster MBTiles from GeoTiff](https://github.com/UrbanSystemsLab/raster-to-mbtiles)
* [Fulcrum: How do I create offline layers using MBTiles?](https://help.fulcrumapp.com/en/articles/4351972-how-do-i-create-offline-layers-using-mbtiles)
* [Neonscience: Merging GeoTIFF Files to Create a Mosaic](https://www.neonscience.org/resources/learning-hub/tutorials/merge-lidar-geotiff-py)
* [StackExchange: How do I combine GeoTIFF files covering the same area but storing different data into a single file?](https://gis.stackexchange.com/questions/325748/how-do-i-combine-geotiff-files-covering-the-same-area-but-storing-different-data)
* [Penn State College: Walkthrough: Overlaying a WMS on a tiled map with Leaflet](https://www.e-education.psu.edu/geog585/node/765)
* [UrbanSystemsLab: Tileserver](https://urbansystemslab.gitbooks.io/cookbook/content/methods/tileserver.html)
* [Ralph's Blog: OpenMapTiles, a self-hosted map tile server with OpenStreetMap data](https://golb.hplar.ch/2018/07/self-hosted-tile-server.html)
